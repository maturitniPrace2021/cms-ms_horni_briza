# CMS-MŠ Horní Bříza
Vytvořte kompletní CMS pro MŠ Horní Bříza. Na webu budou dostupné všechny informace o MŠ Horní Bříza, kontakty, vybavení, fotogalerie, události s možností rezervace, novinky, zprávy z akcí s diskusí.  Veřejnost bude moci kontaktovat MŠ přes formulář rovnou z webu. 

Adresa URL na webovou aplikaci: http://horni-briza.epizy.com
Adresa URL na správce: http://horni-briza.epizy.com/pages/login.php
Přístup do správy:
Jméno: admin
Heslo: meowdy
