-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 16. dub 2021, 17:48
-- Verze serveru: 5.7.17
-- Verze PHP: 7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `database`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `akce`
--

CREATE TABLE `akce` (
  `id` int(11) NOT NULL,
  `nazev` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `misto` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `obsah` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `akce`
--

INSERT INTO `akce` (`id`, `nazev`, `datum`, `misto`, `obsah`) VALUES
(7, ' Test2 hehe', '2021-05-09', 'nikde', NULL),
(8, ' Test2', '2021-05-13', 'nikde', NULL),
(9, ' Test2ffffffffffffffffffffffffffffffffffffffffffffffffffffffkasjdfhdkajshdfkaf asdfjaslkdfjals asdfjalksdfjalskjfd laksjdfaljflak jlakdjfasdjfalksdfj lasdjflkasfjdlak assfhkashfkjahdfksfdhkajsdfhaksfdhaksfhak', '2021-05-09', 'nikde', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE `articles` (
  `id` int(30) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `content` longtext COLLATE utf8_czech_ci NOT NULL,
  `page` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `user` varchar(30) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`id`, `name`, `content`, `page`, `user`) VALUES
(1, 'Naše škola', '<p>Provoz 1. mateřsk&eacute; &scaron;koly byl zah&aacute;jen 1. 2. 1940 v budově b&yacute;val&eacute; obecn&iacute; &scaron;koly v Horn&iacute; Bř&iacute;ze, ve kter&eacute; byla zah&aacute;jena v&yacute;uka 10. 11. 1890. Mateřsk&aacute; &scaron;kola měla 2 oddělen&iacute; až do r. 1978, kdy byla z důvodu velk&eacute;ho počtu dět&iacute; otevřena jedna tř&iacute;da v nedalek&eacute; budově &bdquo;Sokolovny&ldquo;. Zde byl provoz až do r. 1986, kdy je ke st&aacute;vaj&iacute;c&iacute; &scaron;kole přistavěna nov&aacute; budova. Od r. 1995 je mateřsk&aacute; &scaron;kola roz&scaron;&iacute;řena o jednu tř&iacute;du, kter&aacute; je zř&iacute;zena jako odloučen&eacute; pracovi&scaron;tě v 1. patře budovy Hasičsk&eacute; zbrojnice, Tř. 1. m&aacute;je 497. Ve &scaron;koln&iacute;m roce 2000-2001 byl zah&aacute;jen provoz speci&aacute;ln&iacute; tř&iacute;dy pro děti s vadami řeči.</p>\r\n<p>Mateřsk&aacute; &scaron;kola se nach&aacute;z&iacute; v centru Horn&iacute; Bř&iacute;zy &ndash; v doln&iacute; č&aacute;sti města. K v&yacute;chovně vzděl&aacute;vac&iacute; pr&aacute;ci je možno využ&iacute;t i bl&iacute;zk&eacute; př&iacute;rody (lesy, louky, potok).</p>\r\n<img id=\"budova\" src=\"img/msbudova.jpeg\" alt=\"obr&aacute;zek\" />\r\n<p>Mateřsk&aacute; &scaron;kola je čtyřtř&iacute;dn&iacute;, tři tř&iacute;dy jsou v hlavn&iacute; budově &scaron;koly, jedna tř&iacute;da v objektu HZ, vzd&aacute;len&eacute;m od M&Scaron; asi 300 m .V př&iacute;zem&iacute; hlavn&iacute; budovy jsou &scaron;atny dět&iacute; , &scaron;atna zaměstnanců, ředitelna, &uacute;klidov&aacute; komora a jedna tř&iacute;da pro nejmen&scaron;&iacute; děti. V prvn&iacute;m patře jsou dvě tř&iacute;dy, logopedick&aacute; pracovna, pr&aacute;delna a kabinety. Každ&aacute; tř&iacute;da m&aacute; svou hernu, ve kter&eacute; se rozkl&aacute;daj&iacute; leh&aacute;tka, prostory pro ukl&aacute;d&aacute;n&iacute; hraček a metodick&yacute;ch pomůcek, soci&aacute;ln&iacute; zař&iacute;zen&iacute;, kuchyňku pro př&iacute;pravu vyd&aacute;van&yacute;ch j&iacute;del.&nbsp; &nbsp; &nbsp;Na v&yacute;zdobě &scaron;koly se pod&iacute;l&iacute; sv&yacute;mi pracemi děti a učitelky.</p>\r\n<p>Logopedick&aacute; pracovna je v ponděl&iacute; , ve středu, ve čtvrtek a v p&aacute;tek využ&iacute;v&aacute;na logopedick&yacute;mi asistentkami M&Scaron;, 1x za čtrn&aacute;ct dn&iacute; v &uacute;ter&yacute; je zde logopedick&aacute; ordinace Paedr. B. Gruberov&eacute; od 7.00 hod. do 17. 00 hod. pro potřeby dět&iacute; na&scaron;&iacute; M&Scaron;, odpoledne pro děti okoln&iacute;ch M&Scaron; a Z&Scaron; Horn&iacute; Bř&iacute;za.</p>\r\n<p>Za hlavn&iacute; budovou M&Scaron; je velk&aacute; &scaron;koln&iacute; zahrada, kter&aacute; je situov&aacute;na do klidn&eacute; ulice. Souč&aacute;st&iacute; &scaron;koly je &scaron;koln&iacute; j&iacute;delna v př&iacute;zem&iacute; budovy, kter&aacute; je průběžně vybavov&aacute;na tak, aby odpov&iacute;dala stanoven&yacute;m hygienick&yacute;m podm&iacute;nk&aacute;m. V&yacute;dej stravy je pavil&oacute;nov&yacute;.Vyt&aacute;pěn&iacute; &scaron;koly je vlastn&iacute; plynovou kotelnou.</p>\r\n<p>Tř&iacute;da odloučen&eacute;ho pracovi&scaron;tě se nach&aacute;z&iacute; v prvn&iacute;m patře budovy HZ ( vzd&aacute;len&eacute; od hlavn&iacute; budovy M&Scaron; asi 300 m ), m&aacute; svoji hernu, ložnici, &scaron;atnu dět&iacute;, metodick&yacute; kabinet, soci&aacute;ln&iacute; zař&iacute;zen&iacute;, kuchyňku pro př&iacute;pravu vyd&aacute;van&yacute;ch j&iacute;del ( strava se dov&aacute;ž&iacute; ze &scaron;koln&iacute; j&iacute;delny z hlavn&iacute; budovy) a vařen&iacute; tepl&yacute;ch n&aacute;pojů. Zde je vyt&aacute;pěn&iacute; centr&aacute;ln&iacute; městskou kotelnou. Od roku 2015 mohou děti využ&iacute;vat zahr&aacute;dku za HZ. V roce 2020 na n&iacute; přibyly hern&iacute; prvky.</p>\r\n<p>Finančn&iacute; prostředky na provoz &scaron;koly poskytuje zřizovatel M&Scaron;. Rodiče se pod&iacute;l&iacute; na financov&aacute;n&iacute; provozu platbou &scaron;koln&eacute;ho, kter&eacute; je stanoveno rozhodnut&iacute;m ředitelky &scaron;koly, v souladu platn&yacute;ch &scaron;kolsk&yacute;ch z&aacute;konů vždy na jeden &scaron;koln&iacute; rok,&nbsp; na 350,- Kč měs&iacute;čně.</p>\r\n<p>M&scaron; spolupracuje se zřizovatelem M&Scaron; - městem Horn&iacute; Bř&iacute;za, d&aacute;le se z&aacute;kladn&iacute; &scaron;kolou v Horn&iacute; Bř&iacute;ze, z&aacute;kl. uměleckou &scaron;kolou, městskou knihovnou, sborem dobrovoln&yacute;ch hasičů. svazem chovatelů, Polici&iacute; ČR, pediatrem, Zoo Plzeň, z&aacute;chr. stanic&iacute; živočichů a&nbsp; s m&iacute;stn&iacute;m Sokolem.</p>', 'skola', 'ucitel'),
(16, 'Povinné předškolní vzdělání', '<h3><strong>1. VYMEZEN&Iacute; POVINN&Eacute;HO PŘED&Scaron;KOLN&Iacute;HO VZDĚLN&Aacute;V&Aacute;N&Iacute;</strong></h3>\r\n<p>-&nbsp;<strong>Novela &scaron;kolsk&eacute;ho z&aacute;kona, z&aacute;kon č. 178/2016 Sb. zav&aacute;d&iacute; s &uacute;činnost&iacute; od z&aacute;ř&iacute; 2017 povinn&eacute; před&scaron;koln&iacute; vzděl&aacute;v&aacute;n&iacute; pro děti, kter&eacute; do 31. srpna dos&aacute;hnou věku pěti let.<br /></strong>-&nbsp;<strong>Z&aacute;pis je povinn&yacute; pro děti, pokud je&scaron;tě do mateřsk&eacute; &scaron;koly nedoch&aacute;zej&iacute;.</strong><br />- D&iacute;tě, pro kter&eacute; je před&scaron;koln&iacute; vzděl&aacute;v&aacute;n&iacute; povinn&eacute;, se vzděl&aacute;v&aacute;n&iacute; se&nbsp;<strong>sp&aacute;dov&eacute; mateřsk&eacute; &scaron;kole</strong>, pokud se zakonn&yacute; z&aacute;stupce nerozhodl pro jinou mateřskou &scaron;kolu nebo pro individiu&aacute;ln&iacute; vzděl&aacute;v&aacute;n&iacute; d&iacute;těte.<br />- Povinn&eacute; před&scaron;koln&iacute; vzděl&aacute;v&aacute;n&iacute; trv&aacute; 1 &scaron;koln&iacute; rok, (při odkladu &scaron;koln&iacute; doch&aacute;zky 2 roky) a je v obou př&iacute;padech&nbsp;<strong>bezplatn&eacute;</strong>.<br />- Před&scaron;koln&iacute; vzděl&aacute;v&aacute;n&iacute;&nbsp;<strong>nelze ukončit</strong>&nbsp;dle &sect; 35 &scaron;kolsk&eacute;ho z&aacute;kona, pokud je vzděl&aacute;v&aacute;n&iacute; pro d&iacute;tě povinn&eacute;,&nbsp;<strong>povinn&eacute; před&scaron;koln&iacute; vzděl&aacute;v&aacute;n&iacute; je ukončeno zač&aacute;tkem povinn&eacute; &scaron;koln&iacute; doch&aacute;zky</strong>.<br />- Pokud je pro d&iacute;tě před&scaron;koln&iacute; vzděl&aacute;v&aacute;n&iacute; povinn&eacute;,&nbsp;<strong>nepožaduje</strong>&nbsp;&scaron;kola při přij&iacute;mac&iacute;m ř&iacute;zen&iacute;&nbsp;<strong>doklad o očkov&aacute;n&iacute;</strong>.<br />- Děti s odkladem &scaron;koln&iacute; doch&aacute;zky se mohou nově jako jedin&eacute; vzděl&aacute;vat v př&iacute;pravn&eacute; tř&iacute;dě z&aacute;kladn&iacute; &scaron;koly.</p>\r\n<h3><strong>2. DOCH&Aacute;ZKA A ČASOV&Eacute; ROZPĚT&Iacute; POVINN&Eacute;HO VZDĚL&Aacute;V&Aacute;N&Iacute;</strong></h3>\r\n<p><strong style=\"color: #e03e2d;\">DOCH&Aacute;ZKA JE POVINN&Aacute;<br /></strong>- V pracovn&iacute;ch dnech v časov&eacute;m rozsahu&nbsp;<strong>8:00h - 12:00h</strong>.<br />- Opakov&eacute; pozdn&iacute; př&iacute;chody budou hodnoceny jako&nbsp;<strong>neomluven&aacute; absence</strong>.<br />-&nbsp;<strong>Zůst&aacute;v&aacute; pr&aacute;vo</strong>&nbsp;d&iacute;těte vzděl&aacute;vat se v mateřsk&eacute; &scaron;kole&nbsp;<strong>po celou dobu provozu &scaron;koly</strong>.<br />-&nbsp;<strong>Doch&aacute;zka d&iacute;těte denně eviduje</strong>&nbsp;/tiskopis na tř&iacute;dě&nbsp;&nbsp;&bdquo;<strong>Měs&iacute;čn&iacute; přehled doch&aacute;zky</strong>&ldquo;/.<br /><span style=\"color: #e03e2d;\"><strong>DOCH&Aacute;ZKA JE NEPOVINN&Aacute;</strong></span><br />- Ve zbyl&eacute;m provozn&iacute;m čase M&Scaron; /<strong>6:15h - 8:00h, 12:00h - 16:30h</strong>/.<br />- Ve dnech, kter&eacute; připadaj&iacute; na obdob&iacute;&nbsp;<strong>&scaron;koln&iacute;ch pr&aacute;zdnin</strong>, vyhl&aacute;&scaron;en&yacute;ch M&Scaron;MT (podzimn&iacute;ch pr&aacute;zdniny, v&aacute;nočn&iacute; pr&aacute;zdniny, pololetn&iacute; pr&aacute;zdniny, jarn&iacute; pr&aacute;zdniny, velikonočn&iacute; pr&aacute;zdniny, letn&iacute; pr&aacute;zdniny).</p>', 'vzdelani', 'ucitel'),
(2, 'Uzavření MŠ Covid 19', '<p>V&aacute;žen&iacute; rodiče,</p>\r\n<p><strong>na z&aacute;kladě domluvy ředitelky M&Scaron; a Krajsk&eacute; hygienick&eacute; stanice se Mateřsk&aacute; &scaron;kola Horn&iacute; Bř&iacute;za ves od 26. 02. 2021 do 5. 03. 2021 UZAV&Iacute;R&Aacute;.</strong><br /><strong>V př&iacute;loze naleznete ofici&aacute;ln&iacute; vyj&aacute;dřen&iacute; KHS.&nbsp;</strong></p>\r\n<p>K o&scaron;etřovn&eacute;mu&nbsp; - st&aacute;hnete si <a href=\"https://eportal.cssz.cz/web/portal/-/tiskopisy/zoppd-m\">zde</a>, vypln&iacute;te a odevzd&aacute;te zaměstnavateli .V př&iacute;loze naleznete potvrzen&iacute; o uzavřen&iacute; M&Scaron;, kter&eacute; někteř&iacute; zaměstnavatel&eacute; chtěj&iacute;.&nbsp;</p>\r\n<p>Ve&scaron;ker&eacute; info k o&scaron;etřovn&eacute;mu <a href=\"https://www.cssz.cz/web/cz/aktualni-informace-k-osetrovnemu\">zde</a>&nbsp;<br />V př&iacute;padě dotazů se obraťte na ředitelku M&Scaron;, tel. 723 608 411.</p>', 'info', 'ucitel'),
(4, 'Logopedie', '<p>Po dobu uzavřen&iacute; Mateřsk&eacute; &scaron;koly, dle nař&iacute;zen&iacute; vl&aacute;dy od 1.3.2021, nebude v M&Scaron; poskytov&aacute;na Logopedick&aacute; p&eacute;če.</p>\r\n<p>Po telefonick&eacute; domluvě na telefonn&iacute;m č&iacute;sle&nbsp; 605 811 131 s pan&iacute; logopedkou, je možn&eacute; objednat n&aacute;v&scaron;těvu v Plzeňsk&eacute; ordinaci,</p>\r\n<p>adresa Fibichova 2, Plzeň.</p>', 'info', 'ucitel'),
(5, 'Uzavření MŠ - Vládní nařízení', '<p><strong>Mateřsk&aacute; &scaron;kola je z rozhodnut&iacute; vl&aacute;dy ČR od 1.3.2021 uzavřena.</strong></p>\r\n<p><strong>O opětovn&eacute;m uveden&iacute; mateřsk&eacute; &scaron;koly do provozu v&aacute;s budeme informovat.</strong></p>', 'info', 'ucitel'),
(6, 'Zápis do MŠ', '<p>V&aacute;žen&iacute; rodiče,</p>\r\n<p>z&aacute;pis k před&scaron;koln&iacute;mu vzděl&aacute;v&aacute;n&iacute; proběhne dle &sect; 34 &scaron;kolsk&eacute;ho z&aacute;kona v obdob&iacute; od <strong>2. května do 16. května 2021</strong>. Bliž&scaron;&iacute; informace uveřejn&iacute;me v měs&iacute;ci dubnu.</p>', 'info', 'ucitel'),
(8, 'Organizace dne', '<p><strong>Organizace</strong> průběhu dne je navržena r&aacute;mcově, aby ji bylo možn&eacute; dle potřeb a okolnost&iacute; upravovat a měnit. Snaž&iacute;me se o vyv&aacute;žen&yacute; poměr spont&aacute;nn&iacute;ch a ř&iacute;zen&yacute;ch činnost&iacute;. Počet dět&iacute; ve tř&iacute;d&aacute;ch se ř&iacute;d&iacute; platnou legislativou.</p>\r\n<p>Tř&iacute;dy jsou věkově sm&iacute;&scaron;en&eacute;. R&aacute;no (do cca 7:00 hod) a odpoledne(od 15:30 hod.) jsou tř&iacute;dy spojeny, protože je v M&Scaron; již&nbsp; men&scaron;&iacute; počet dět&iacute;. Po obědě děti odpoč&iacute;vaj&iacute;, dětem s men&scaron;&iacute; potřebou sp&aacute;nku jsou po kr&aacute;tk&eacute;m odpočinku&nbsp; nab&iacute;zeny individu&aacute;ln&iacute; klidov&eacute; činnosti.</p>\r\n<p>Pravideln&eacute; aktivity: logopedick&aacute; prevence, relaxace v soln&eacute; jeskyni a předplaveck&aacute; v&yacute;uka prob&iacute;haj&iacute; v dopoledn&iacute;ch hodin&aacute;ch.</p>\r\n<p>V odpoledn&iacute;ch hodin&aacute;ch- kroužky - <strong>Vzhledem k situaci Covid 19 kroužky prozat&iacute;m v M&Scaron; neprob&iacute;haj&iacute;.</strong></p>\r\n<p><strong>Režim dne</strong> v mateřsk&eacute; &scaron;kole je navržen z důvodů, že děti před&scaron;koln&iacute;ho věku by měly m&iacute;t stanoven (alespoň r&aacute;mcově) sled činnosti a ritu&aacute;lů, kter&eacute; se v průběhu dne v mateřsk&eacute; &scaron;kole stř&iacute;daj&iacute;:</p>\r\n<p>&nbsp;</p>\r\n<p><strong>6:00 &ndash; 9:15</strong>: sch&aacute;zen&iacute; dět&iacute;, rann&iacute; hry a činnosti dět&iacute; podle jejich př&aacute;n&iacute;, individu&aacute;ln&iacute; pr&aacute;ce s dětmi<br /><strong>8:30 - 9:15</strong>: hygiena,&nbsp; průběžn&aacute; přesn&iacute;d&aacute;vka dle potřeby a chuti dět&iacute;<br /><strong>9:15 -10:00</strong>:&nbsp; z&aacute;měrn&eacute; plněn&iacute; činnost&iacute; rozv&iacute;jej&iacute;c&iacute; tělesn&eacute;, smyslov&eacute;, řečov&eacute;, manipulačn&iacute; a estetick&eacute;&nbsp; str&aacute;nky dětsk&eacute; osobnosti (pr&aacute;ce se skupinou dět&iacute;, individu&aacute;ln&iacute;, popř. front&aacute;ln&iacute;), relaxačn&iacute;, průpravn&aacute; cvičen&iacute;, jazykov&eacute; chvilky<br /><strong>10:00 - 12:00</strong>: př&iacute;prava na pobyt venku, pobyt venku (podle činnost&iacute; a počas&iacute; dř&iacute;věj&scaron;&iacute; odchod)<br /><strong>12:00 - 12:30</strong>: hygiena, př&iacute;prava na oběd, oběd<br /><strong>12:30 - 14:15</strong>: hygiena, odpočinek (sp&aacute;nek), četba učitelky, převl&eacute;k&aacute;n&iacute;<br /><strong>13:30 - 14:30</strong>: děti, kter&eacute; nesp&iacute;, klidn&eacute; činnosti ve tř&iacute;dě dle jejich z&aacute;jmu nebo individu&aacute;ln&iacute; pr&aacute;ce n&aacute;prava v&yacute;slovnosti, grafomotorika, apod.&nbsp;<br /><strong>14:15 - 15:00</strong>: hygiena, průběžn&aacute; svačina<br /><strong>14:30 - 16:30</strong>: hrav&eacute; a relaxačn&iacute; činnosti podle volby dět&iacute; do jejich odchodu domů</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Odchod dět&iacute; z M&Scaron;:</strong></span>&nbsp;</p>\r\n<p>Po obědě doporučeno ve <strong>12:30</strong> hodin.<br />Odpoledne doporučeno od <strong>14:45</strong> hodin &ndash; individu&aacute;lně podle potřeby rodičů.<br />Tuto organizaci dne lze změnit dle potřeby, kter&aacute; vyplyne z moment&aacute;ln&iacute; situace v průběhu dne.</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Činnosti pravidelně se opakuj&iacute;c&iacute; v průběhu t&yacute;denn&iacute;ho režimu M&Scaron;:</strong></span></p>\r\n<p>- individu&aacute;ln&iacute; n&aacute;prava v&yacute;slovnosti<br />- kroužek angličtiny<br />- relaxačn&iacute; pobyt v soln&eacute; jeskyni (zpravidla leden-březen)<br />- před plaveck&aacute; v&yacute;uka (zpravidla duben-červen)<br />- ledn&iacute; bruslen&iacute;<br />- keramika</p>', 'organizace', 'ucitel'),
(9, 'Kontakty', '<h2>1. Mateřsk&aacute; &scaron;kola Horn&iacute; Bř&iacute;za - ves</h2>\r\n<p>Okres Plzeň - sever, př&iacute;spěvkov&aacute; organizace<br />Tř&iacute;da 1. M&aacute;je 56, 330 12 Horn&iacute; Bř&iacute;za<br />IČO: 60610778</p>\r\n<p>Ředitelka: <strong>Jitka Čepcov&aacute;</strong><br />Telefon: <strong>723 608 411</strong><br />E-mail: <strong>1.MShornibrizaves@seznam.cz</strong><br />Datov&aacute; schr&aacute;nka: <strong>p47mvwr</strong></p>\r\n<p><span style=\"text-decoration: underline;\">&Uacute;ředn&iacute; hodiny ředitelky:</span><br />Ponděl&iacute;: 11:00-12:00<br />Středa: 11:00-12:00<br />Po domluvě osobn&iacute;, či po telefonu <strong>723 608 411</strong> je možn&eacute; domluvit schůzku</p>\r\n<p>Vedouc&iacute; učitelka v době nepř&iacute;tomnosti ředitelky: <strong>Rychetsk&aacute; Lenka</strong><br />Z&aacute;stupkyně ředitelky a vedouc&iacute; učitelka na OP: <strong>Bc.</strong> <strong>Juhov&aacute; Jana</strong></p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Tř&iacute;da Beru&scaron;ek</strong></span><br />Telefon: <strong>602 641 577<br /></strong>E-mail:&nbsp;<strong>msberuskyhb@gmail.com</strong><br />Učitelky: Rychetsk&aacute; Lenka, Preisingerov&aacute; Kateřina, DiS.<br />&Scaron;koln&iacute; asistent: Bourke Krist&yacute;na</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Třida Papou&scaron;ků</strong></span><br />Telefon: <strong>602 641 564<br /></strong>E-mail:&nbsp;<strong>mspapouscihb@gmail.com<br /></strong>Učitelky: Mgr. Pol&iacute;vkov&aacute; Zděnka, Soprov&aacute; Marcela<br />Asistent pedagoga: Bc. Pol&aacute;kov&aacute; Magdalena</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Tř&iacute;da Mot&yacute;lků</strong></span><br />Telefon: <strong>602 641 650</strong><br />E-mail: <strong>msmotylcihb@gmail.com</strong><br />Učitelky: Čepcov&aacute; Jitka, Pol&aacute;kov&aacute; Renata, Bourke Krist&yacute;na</p>\r\n<p><strong>Odloučen&eacute; pracovi&scaron;tě</strong><br />Adresa: Tř. 1. m&aacute;je 497</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>Tř&iacute;da Dr&aacute;čků</strong></span><br />Telefon: <strong>737 580 024</strong><br />E-mail: <strong>dracci-ms@seznam.cz</strong><br />Z&aacute;stupkyně ředitelky: Bc. Juhov&aacute; Jana<br />Učitelky: Bc. Pioreck&aacute; Kateřina, Bc. Juhov&aacute; Jana, Bourke Krist&yacute;na</p>\r\n<p><span style=\"text-decoration: underline;\"><strong>&Scaron;koln&iacute; j&iacute;delna</strong></span><br />Adresa: Tř. 1. m&aacute;je 56<br />Vedouc&iacute; &Scaron;J: Červen&aacute; Gabriela<br />Telefon: <strong>605 536 212</strong><br />E-mail: <strong>1.MSjidelna@seznam.cz</strong></p>\r\n<p><br /><a href=\"https://mapy.cz/letecka?x=13.3561125&amp;y=49.8403426&amp;z=17&amp;l=1&amp;source=addr&amp;id=9200527\">Najdete n&aacute;s zde</a><br />V doln&iacute; č&aacute;sti města u velk&eacute; zat&aacute;čky, bl&iacute;zko Radnice<br />odloučen&eacute; pracovi&scaron;tě - 300m od hlavn&iacute; budovy M&Scaron;, př&iacute;mo za radnic&iacute;</p>', 'kontakty', 'ucitel');

-- --------------------------------------------------------

--
-- Struktura tabulky `class`
--

CREATE TABLE `class` (
  `id` int(30) NOT NULL,
  `className` varchar(30) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `diskuze`
--

CREATE TABLE `diskuze` (
  `id` int(11) NOT NULL,
  `context_id` int(11) NOT NULL,
  `author` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `message` text COLLATE utf8_czech_ci NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `diskuze`
--

INSERT INTO `diskuze` (`id`, `context_id`, `author`, `message`, `created_at`) VALUES
(14, 8, 'Test unlogged', 'lol', '2021-04-15 01:19:04'),
(17, 8, 'Test logged', 'ayaya', '2021-04-15 01:19:58'),
(18, 8, 'Test 3', 'just for fun', '2021-04-15 01:20:12'),
(19, 8, 'Test 4 ', 'už plně funkční', '2021-04-15 01:21:01'),
(20, 7, 'testing', 'testování i tady haha', '2021-04-15 01:32:13'),
(21, 7, 'test 2', 'potřebuju vidět mezeru', '2021-04-15 01:32:24'),
(22, 9, 'help', 'sleep', '2021-04-15 02:18:53');

-- --------------------------------------------------------

--
-- Struktura tabulky `hosts`
--

CREATE TABLE `hosts` (
  `id` int(11) NOT NULL,
  `jmeno` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `prijmeni` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` int(255) DEFAULT NULL,
  `pocet` int(255) DEFAULT NULL,
  `akce` int(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `hosts`
--

INSERT INTO `hosts` (`id`, `jmeno`, `prijmeni`, `email`, `telefon`, `pocet`, `akce`) VALUES
(9, 'Test', 'Testovič', 'test@test.test', 731598454, 2, 9),
(10, 'Test', 'Testovič', 'test@test.test', 731598454, 2, 9),
(11, 'Test', 'Testovič', 'test@test.test', 731598454, 2, 9),
(12, 'test2', 'testovičoas', 'kekew@lul.com', 735648954, 8, 9);

-- --------------------------------------------------------

--
-- Struktura tabulky `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `images`
--

INSERT INTO `images` (`id`, `file_name`, `page`) VALUES
(4, 'img-20201014-wa0000.jpg', 'papousci'),
(5, 'img-20201014-wa0002.jpg', 'papousci'),
(6, 'img_20201102_151056.jpg', 'berusky'),
(7, 'img_20201102_151104.jpg', 'berusky'),
(8, 'leden-dracci.jpg', 'dracci'),
(9, 'dracci2.jpg', 'dracci'),
(10, 'dracci3.jpg', 'dracci'),
(11, 'motylci.jpg', 'motylci'),
(12, 'motylci2.jpg', 'motylci'),
(21, 'leden-dracci.jpg', '6'),
(22, 'motylci.jpg', '4');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', 'meowdy'),
(2, 'duonga', 'Lukitakino21'),
(3, 'juhovat', 'meowdy19826450'),
(22, 'Marekek', '$2y$10$j.MY/P/o7hMU/uzRyDZh1unuDbF2Eo7TFlQVZlLrow8f/M3nmVSsG'),
(23, '', '$2y$10$i/je9Ln5//9RUAcb/b56n.uXX6z7j.oX6SJvrig0a83o/9BL0n2SW');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `akce`
--
ALTER TABLE `akce`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `diskuze`
--
ALTER TABLE `diskuze`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `hosts`
--
ALTER TABLE `hosts`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `akce`
--
ALTER TABLE `akce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pro tabulku `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pro tabulku `diskuze`
--
ALTER TABLE `diskuze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pro tabulku `hosts`
--
ALTER TABLE `hosts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pro tabulku `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
