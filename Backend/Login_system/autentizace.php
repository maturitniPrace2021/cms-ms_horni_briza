<?php
session_start();
include '../Administration/config.php';
$sqlconn = new mysqli(dbserver, dbname, dbpass, tbname);
ini_set("session.cookie_httponly", 1);

if ($stm = $sqlconn->prepare("SELECT id, password FROM users WHERE username = ?")) {
    $stm->bind_param('s', $_POST['username']);
    $stm->execute();
    $stm->store_result();
    if ($stm->num_rows > 0) {
        $stm->bind_result($id, $password);
        $stm->fetch();
        $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        if (password_verify($password, $hash)) {
            session_regenerate_id();
            $_SESSION['loggedin'] = true;
            $_SESSION['name'] = $_POST['username'];
            $_SESSION['id'] = $id;
            header('Location: ../Administration/seznam.php');
        } else {
            header('Location: ../../pages/login.php?msg=Špatně zadané heslo');
        }
    } else {
        header('Location: ../../pages/login.php?msg=Špatně zadané přihlašovací jméno');
    }
    $stm->close();
}


