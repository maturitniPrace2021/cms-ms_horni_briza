<?php
include "config.php";
$nazev = $_POST["nazev"];
$obsah = $_POST["obsah"];
$id = $_POST["id"];

$sqlCon = new mysqli(dbserver, dbname, dbpass, tbname);
$sqlCon->set_charset('UTF8');
$sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');

$sql1 = "UPDATE articles SET `name` = '$nazev', content = '$obsah' WHERE id = '$id'";
$upd = $sqlCon->query($sql1);

$statusMsg = '';
$targetDir = "images/";
$fileName = basename($_FILES["file"]["name"]);
$targetFilePath = $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
$insert = "";
if (isset($_POST["tlacitko"]) && !empty($_FILES["file"]["name"])) {
    // Allow certain file formats
    $allowTypes = array('jpg', 'png', 'jpeg');
    if (in_array($fileType, $allowTypes)) {
        // Upload file to server
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)) {
            // Insert image file name into database
            $insert = $sqlCon->query("INSERT into images (`file_name`,`page`) VALUES ('" . $fileName . "','$id')");
        } else {
            $statusMsg = "Sorry, there was an error uploading your file.";
        }
    } else {
        $statusMsg = 'Sorry, only JPG, JPEG, PNG files are allowed to upload.';
    }
} else {
    header("location:seznam.php");
    exit;
}

// Display status message
echo $statusMsg;

if ($upd && $insert) {
    mysqli_close($sql1);
    header("location:seznam.php");
    exit;
}else{
    echo (mysqli_error($sqlCon));
}