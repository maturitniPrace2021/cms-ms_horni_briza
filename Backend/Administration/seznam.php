<?php
session_start();
if (!isset($_SESSION['loggedin'])) {
    header('Location: ../../pages/login.php');
    exit;
}else{
require_once "../../src/latte.php";
$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

include "config.php";
$sqlCon = new mysqli(dbserver, dbname, dbpass, tbname);
$sqlCon->set_charset('UTF8');
$sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');

$sql = "SELECT * FROM articles ORDER BY id";
$result = $sqlCon->query($sql);

$sql2 = "SELECT * FROM akce ORDER BY id";
$result2 = $sqlCon->query($sql2);

$params = [
    'result' => $result,
    'result2' => $result2,
];
$latte->render('../../template/seznam.latte', $params);
}