<?php
if(!file_exists('fpdf.php')){

echo " Place fpdf.php file in this directory before using this page.<br>
Read the instructions at <a href=https://www.plus2net.com/php_tutorial/pdf-data-student.php>https://www.plus2net.com/php_tutorial/pdf-data-student.php</a> ";
exit;
}

if(!file_exists('font')){
	echo " Place font directory in this directory before using this page. ";
exit;
}

require "../Administration/config.php"; // connection to database
$sqlCon = new mysqli (dbserver, dbname, dbpass, tbname);
$sqlCon->set_charset('UTF8');
$sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');
$count="select * from hosts WHERE akce='$_GET[id]'"; // SQL to get 10 records
require('fpdf.php');
$pdf = new FPDF();
$pdf->AddPage();

$width_cell=array(35,35,70,30,20);
$pdf->SetFont('Arial','B',16);

$pdf->SetFillColor(193,229,252); // Background color of header
// Header starts ///
$pdf->Cell($width_cell[0],10,'jmeno',1,0); // Second header column
$pdf->Cell($width_cell[1],10,'prijmeni',1,0); // Third header column
$pdf->Cell($width_cell[2],10,'email',1,0); // Fourth header column
$pdf->Cell($width_cell[3],10,'telefon',1,0); // Third header column
$pdf->Cell($width_cell[4],10,'pocet',1,1); // Third header column

//// header ends ///////

$pdf->SetFont('Arial','',12);
$pdf->SetFillColor(235,236,236); // Background color of header
$fill=false; // to give alternate background fill color to rows

/// each record is one row  ///
foreach ($sqlCon->query($count) as $row) {
$pdf->Cell($width_cell[0],10,$row['jmeno'],1,0);
$pdf->Cell($width_cell[1],10,$row['prijmeni'],1,0);
$pdf->Cell($width_cell[2],10,$row['email'],1,0);
$pdf->Cell($width_cell[3],10,$row['telefon'],1,0);
$pdf->Cell($width_cell[4],10,$row['pocet'],1,1);
$fill = !$fill; // to give alternate background fill  color to rows
}
/// end of records ///

$pdf->Output();

?>
