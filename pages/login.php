<!DOCTYPE html>
<html lang="cs" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../img/mslogofavismol.png">
    <title></title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style media="screen">
        body {
            margin: 0;
            padding: 0;
            background-color: #06B0DB;
            height: 100vh;
        }

        #login .container #login-row #login-column #login-box {
            margin-top: 35px;
            max-width: 600px;
            height: 320px;
            border: 1px solid #9C9C9C;
            background-color: #EAEAEA;
        }

        #login .container #login-row #login-column #login-box #login-form {
            padding: 20px;
        }

        #login .container #login-row #login-column #login-box #login-form {
            margin-top: auto;
        }
    </style>
</head>
<body>
<div id="login">
    <h3 class="text-center text-white pt-5" style="margin-top: 135px;">Administrace</h3>
    <?php
    if($_GET['msg']){
        echo '<div class ="mx-auto" style="background-color: #f44336;margin-top: 5px;padding:2px;border-radius: 3px;color:white;width:20vw;min-width: 20vw;text-align: center">' . $_GET['msg'] . '</div>';
    }
    ?>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="../Backend/Login_system/autentizace.php" method="post">
                        <h3 class="text-center text-info">Přihlášení</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Uživatelské jméno:</label><br>
                            <input type="text" name="username" id="username" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Heslo:</label><br>
                            <input type="password" name="password" id="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
