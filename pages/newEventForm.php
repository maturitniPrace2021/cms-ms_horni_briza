<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css-backend/style.css">
    <link rel="shortcut icon" href="../img/mslogofavismol.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
    <title>Nová akce</title>
    <style>
    body{
      text-align: center;
      padding-left: 2%;
      padding-right: 2%;
      padding-top: 2.5%;


    }
    label, input, select, a{
      margin: 2px;
    }
    h1{
      color: #06B0DB;
    }
    #sedeokynko{
      border-radius: 10px;
      border: 1px solid grey;
      background-color: #EAEAEA;
      margin: auto;
      width: 25vw;
    }
    </style>
</head>
<body>
<h1>Vytvoření akce</h1>
<hr style="padding-right: 1.5%;">
<br>
<div id="sedeokynko">
<br>
<form action="../Backend/Administration/newEvent.php" method="post">
    <label for="nazev">Zadejte název akce: </label>
    <input type="text" name="nazev" id="nazev" placeholder="Název akce">
        <br><br>
    <label for="datum">Zadejte datum: </label>
    <input type="date" name="datum" id="datum">
        <br><br>
    <label for="misto">Zadejte místo konání akce: </label>
    <input type="text" name="misto" id="misto" placeholder="Místo akce"><br><br>
</div>
    <div>
        <textarea  name="obsah" id="obsah" cols="100" rows="20"></textarea>
    </div>
    <input type="submit" value="Vytvořit akci">
</form>
</body>
</html>
