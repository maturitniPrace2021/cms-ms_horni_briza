<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="../img/mslogofavismol.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
    <title>Změna obsahu</title>
    <?php
    include "../Backend/Administration/config.php";
    $id = $_GET["id"];
    $sqlCon = new mysqli(dbserver, dbname, dbpass, tbname);
    $sqlCon->set_charset('UTF8');
    $sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');

    $sql = "SELECT * FROM articles WHERE id ='$id'";
    $result = mysqli_query($sqlCon, $sql);
    $row = mysqli_fetch_row($result);
    $name = $row[1];
    $content = $row[2];
    ?>
    <style>
        body{
          text-align: left;
          color: black !important;
        }
        h1{
          color: #06B0DB;
        }
    </style>
</head>
<body class=" pt-5 px-5">
<h1>Úprava článku</h1>
<hr>
<br>
<form action="../Backend/Administration/update.php" method="post" enctype="multipart/form-data">
    <div>
        <label class="d-block" for="nazev">Název článku: </label>
        <input class="w-25 p-2" type="text" value="<?php echo $name; ?>" name="nazev" id="nazev">
        <input class="d-none" value="<?php echo $id; ?>" id="id" name="id">
    </div>
    <br>
    <div>
        <label class="d-block" for="file">Přidání profilové fotky: </label>
        <input type="file" name="file" id="file">
    </div>
    <br>
    <div>
        <label class="d-block" for="obsah">Obsah: </label>
        <textarea name="obsah" id="obsah" cols="100" rows="20"><?php echo $content; ?></textarea>
        <br>
    </div>

    <input type="submit" name="tlacitko" value="Uložit">

</form>
<br>
<a href="../Backend/Administration/seznam.php">Zpět</a>
</body>
</html>
