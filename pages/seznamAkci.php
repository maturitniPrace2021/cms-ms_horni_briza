<?php
require_once "../src/latte.php";
$latte = new Latte\Engine;
$latte->setTempDirectory('temp');
include "../Backend/Administration/config.php";
$sqlCon = new mysqli(dbserver, dbname, dbpass, tbname);
$sqlCon->set_charset('UTF8');
$sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');

$sql="SELECT * FROM akce";
$result = $sqlCon->query($sql);

$params = [
    'result'=>$result,
    'BASE_URL' => BASE_URL
];
$latte->render('../template/seznamAkci.latte', $params);
