<?php
session_start();
if (!isset($_SESSION['loggedin'])) {
    header('Location: login.php');
    exit;
}
?>

<!DOCTYPE html>
<html lang="cs" dir="ltr">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/mslogofavismol.png">
    <meta charset="utf-8">
    <title>Náhrání fotky</title>
    <style>
        body{
          padding-top: 10% !important;
          text-align: center;
          padding-left: 2%;
          padding-right: 2%;

        }
        label, input, select, a{
          margin: 2px;
        }
        h1{
          color: #06B0DB;
        }
        #sedeokynko{
          border-radius: 10px;
          border: 1px solid grey;
          background-color: #EAEAEA;
          margin: auto;
          width: 25vw;
        }
    </style>
</head>
<body>
<h1 >Nahrání obrázku do fotogalerie</h1>
<hr>
<br>
<div id="sedeokynko">
<br>
<form action="../Backend/Administration/upload.php" method="post" enctype="multipart/form-data">
    <label for="file">Soubor:</label>
    <input type="file" name="file"><br>
    <label for="page">Složka: </label>
    <select name="page">
        <option value="dracci">Tř. Dráčci</option>
        <option value="motylci">Tř. Motýlci</option>
        <option value="berusky">Tř. Berušky</option>
        <option value="papousci">Tř. Papoušci</option>
        <option value="prace">Práce dětí</option>
        <option value="prostory">Prostory školky</option>
        <option value="akce">Společné akce</option>
    </select>
    <input type="submit" name="tlacitko" value="Nahrát">
</form>
<a href="../Backend/Administration/seznam.php">Zpět</a>
<br>
<br>
</div>

</body>
</html>
