<?php
require_once "../src/latte.php";
$latte = new Latte\Engine;
$latte->setTempDirectory('temp');
include "../Backend/Administration/config.php";

$sqlCon = new mysqli(dbserver, dbname, dbpass, tbname);
$sqlCon->set_charset('UTF8');
$sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');
$query = "SELECT * FROM `images` WHERE `page` = 'papousci'";
$result = $sqlCon->query($query);
$image_url = "../Backend/Administration/images/";
$nadpis="Třída Papoušci";

$params = [
    'result'=>$result,
    'image_url'=>$image_url,
    'nadpis'=>$nadpis,
    'BASE_URL' => BASE_URL
];
$latte->render('../template/template_obrazky.latte', $params);
?>
