<?php
require_once "../src/latte.php";
$latte = new Latte\Engine;
$latte->setTempDirectory('temp');
include '../Backend/Administration/config.php';

$sqlCon = new mysqli (dbserver, dbname, dbpass, tbname);
$sqlCon->set_charset('UTF8');
$sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');

$id = $_GET['id'];

session_start();
$isAdmin = isset($_SESSION['loggedin']);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_GET['komentar'] == 1) {
        if (!array_key_exists('author', $_POST) || !array_key_exists('message', $_POST)) {
            throw new RuntimeException('Chybné parametry');
        }
        $author = $_POST['author'];
        $message = $_POST['message'];
        if (strlen(trim($author)) === 0 || strlen(trim($message)) === 0) {
            throw new RuntimeException('Chybné parametry');
        }

        // TODO @zlutak insert do DB
        $sql = "INSERT INTO diskuze(author, message,context_id) VALUES ('$author','$message','$id')";
        $resultInsert = $sqlCon->query($sql);
    } else if ($_GET['smaz'] == 1 && array_key_exists('komentar_id', $_GET)) {
        if (!$isAdmin) {
            throw new RuntimeException("Nemáte oprávnění", 403);
        }

        $idKomentar = (int)$_GET['komentar_id'];

        // TODO @zlutak Smazani Z DB
        $sql = $sqlCon->query("DELETE FROM diskuze WHERE id='$idKomentar' AND context_id='$id'");
    }
}

$sql2 = " SELECT * FROM akce WHERE id = '$id' ";
$result = $sqlCon->query($sql2) or die($sqlCon->error);
while ($row = $result->fetch_assoc()) {
    $idAkce = $row['id'];
    $nazevAkce = $row['nazev'];
    $obsahAkce = $row['obsah'];
}

$sql3 = "SELECT * FROM diskuze WHERE context_id='$id'";
$result2 = $sqlCon->query($sql3) or die($sqlCon->error);
$arrayDiskuze=[];
while($row2 = $result2->fetch_assoc()) {
    $arrayDiskuze[]=$row2;
}

$params = [
    'akce' => ['id' => $idAkce, 'titulek' => $nazevAkce, 'obsah' => $obsahAkce],
    'diskuze' => $arrayDiskuze,
    //'result2'=>$result2,\
    'is_admin' => $isAdmin,
    'BASE_URL' => BASE_URL
];

$latte->render('../template/detail_akce.latte', $params);
