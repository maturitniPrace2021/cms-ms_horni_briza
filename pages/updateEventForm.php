
<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="../img/mslogofavismol.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
    <title>Změna obsahu</title>
    <?php
    include "../Backend/Administration/config.php";
    $id = $_GET["id"];
    $sqlCon = new mysqli(dbserver, dbname, dbpass, tbname);
    $sqlCon->set_charset('UTF8');
    $sqlCon->query('SET COLLATION_CONNECTION = UTF8_CZECH_CI');

    $sql = "SELECT * FROM akce WHERE id ='$id'";
    $result = mysqli_query($sqlCon, $sql);
    $row = mysqli_fetch_row($result);
    $name = $row[1];
    $datum = $row[2];
    $misto = $row[3];
    $obsah = $row[4];

    ?>
</head>
<body class="text-primary pt-5 px-5">
<h1>Úprava artiklu</h1>
<form action="../Backend/Administration/updateEvent.php" method="post" enctype="multipart/form-data">
    <div>
        <label class="d-block" for="nazev">Název akce:</label>
        <input class="w-25 p-2" type="text" value="<?php echo $name; ?>" name="nazev" id="nazev">
        <input class="d-none" value="<?php echo $id; ?>" id="id" name="id">
    </div>
    <div>
        <label class="d-block" for="datum">Datum:</label>
        <input type="date" value="<?php echo $datum?>" name="datum" id="datum">
    </div>
    <div>
        <label class="d-block" for="misto">Místo:</label>
        <input type="text" value="<?php echo $misto?>" name="misto" id="misto">
    </div>

    <div>
        <textarea  name="obsah" id="obsah" cols="100" rows="20" ><?php echo $obsah?></textarea>
    </div>
    <input type="submit" name="tlacitko" value="Uložit">
</form>

<a href="../Backend/Administration/seznam.php">Zpět</a>
</body>
</html>
